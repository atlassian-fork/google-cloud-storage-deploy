#!/usr/bin/env sh

# Deploy to GCP storage, https://cloud.google.com/storage/
#
# Required globals:
#   KEY_FILE
#   PROJECT
#   BUCKET
#   SOURCE
#
# Optional globals:
#   CACHE_CONTROL
#   CONTENT_DISPOSITION
#   CONTENT_ENCODING
#   CONTENT_LANGUAGE
#   CONTENT_TYPE
#   ACL
#   STORAGE_CLASS
#   EXTRA_ARGS

source "$(dirname "$0")/common.sh"
enable_debug

# mandatory parameters
KEY_FILE=${KEY_FILE:?'KEY_FILE variable missing.'}
PROJECT=${PROJECT:?'PROJECT variable missing.'}
BUCKET=${BUCKET:?'BUCKET variable missing.'}
SOURCE=${SOURCE:?'SOURCE variable missing.'}

info "Setting up environment".

echo "${KEY_FILE}" | base64 -d >> /tmp/key-file.json
run gcloud auth activate-service-account --key-file /tmp/key-file.json --quiet ${gcloud_debug_args}
run gcloud config set project $PROJECT --quiet ${gcloud_debug_args}

# Enable multi threaded
METADATA_STRING="-m"

if [ ! -z "${CACHE_CONTROL}" ]; then
  METADATA_STRING="${METADATA_STRING} -h \"Cache-Control: ${CACHE_CONTROL}\" "
fi

if [ ! -z "${CONTENT_DISPOSITION}" ]; then
  METADATA_STRING="${METADATA_STRING} -h \"Content-Disposition: ${CONTENT_DISPOSITION}\" "
fi

if [ ! -z "${CONTENT_ENCODING}" ]; then
  METADATA_STRING="${METADATA_STRING} -h \"Content-Encoding: ${CONTENT_ENCODING}\" "
fi

if [ ! -z "${CONTENT_LANGUAGE}" ]; then
  METADATA_STRING="${METADATA_STRING} -h \"Content-Language: ${CONTENT_LANGUAGE}\" "
fi

if [ ! -z "${CONTENT_TYPE}" ]; then
  METADATA_STRING="${METADATA_STRING} -h \"Content-Type: ${CONTENT_TYPE}\" "
fi

ARGS_STRING=""

if [ -d "${SOURCE}" ]; then
  ARGS_STRING="${ARGS_STRING} -r "
fi

if [ ! -z "${ACL}" ]; then
  ARGS_STRING="${ARGS_STRING} -a ${ACL} "
fi

if [ ! -z "${STORAGE_CLASS}" ]; then
  ARGS_STRING="${ARGS_STRING} -s ${STORAGE_CLASS} "
fi

ARGS_STRING="${ARGS_STRING} ${EXTRA_ARGS:=""} ${gcloud_debug_args}"

info "Starting deployment to GCP storage..."

run gsutil ${METADATA_STRING} cp ${ARGS_STRING} ${SOURCE} gs://${BUCKET}/

if [ "${status}" -eq 0 ]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
